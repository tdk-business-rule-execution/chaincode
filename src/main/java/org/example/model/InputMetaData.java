package org.example.model;

import java.util.ArrayList;
import java.util.List;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;
import org.json.JSONObject;

import com.owlike.genson.annotation.JsonIgnore;

import static java.nio.charset.StandardCharsets.UTF_8;

@DataType()
public class InputMetaData {
    @Property()
    public String inputId;

    @Property()
    public String inputName;

    @Property()
    public String typeRef;

    @Property()
    public List<String> defaultValues;

    public InputMetaData(String inputId, String inputName, String typeRef, List<String> defaultValues) {
        this.inputId = inputId;
        this.inputName = inputName;
        this.typeRef = typeRef;
        this.defaultValues = defaultValues;
    }

    @JsonIgnore
    public InputMetaData(Input i) {
        inputId = i.getInputId();
        inputName = i.getInputName();
        typeRef = i.getTypeRef();
        defaultValues = i.getDefaultValues();
    }
    public byte[] serialize() {
        String jsonStr = new JSONObject(this).toString();
        return jsonStr.getBytes(UTF_8);
    }

    public String toJSONString() {
        return new JSONObject(this).toString();
    }

     //#region getters,setters

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputName() {
        return inputName;
    }

    public void setInputName(String inputName) {
        this.inputName = inputName;
    }

    public String getTypeRef() {
        return typeRef;
    }

    public void setTypeRef(String typeRef) {
        this.typeRef = typeRef;
    }

    public List<String> getDefaultValues() {
        return defaultValues;
    }

    public void setDefaultValues(List<String> defaultValues) {
        this.defaultValues = defaultValues;
    }

   //#endregion

    
}
