package org.example.model;

import java.util.List;

import javax.annotation.PropertyKey;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;
import org.json.JSONObject;
import static java.nio.charset.StandardCharsets.UTF_8;

import com.owlike.genson.annotation.JsonIgnore;
import com.owlike.genson.annotation.JsonProperty;

@DataType()
public class DataDependencyAsset {

    @Property()
    private String decisionId;
    
    @Property()
    private String decisionOwnerOrganization;

    @Property()
    private List<InputMetaData> inputsMetaData;



    public DataDependencyAsset(@JsonProperty("decisionId") String decisionId,
                @JsonProperty("decisionOwnerOrganization") String decisionOwnerOrganization, @JsonProperty("inputsMetaData") List<InputMetaData> inputsMetaData) {
        this.decisionId = decisionId;
        this.decisionOwnerOrganization = decisionOwnerOrganization;
        this.inputsMetaData = inputsMetaData;
    }

    public byte[] serialize() {
        String jsonStr = new JSONObject(this).toString();
        return jsonStr.getBytes(UTF_8);
    }

    public String toJSONString() {
        return new JSONObject(this).toString();
    }

    //#region getters,setters

    public String getDecisionId() {
        return decisionId;
    }

    public void setDecisionId(String decisionId) {
        this.decisionId = decisionId;
    }

    public String getDecisionOwnerOrganization() {
        return decisionOwnerOrganization;
    }

    public void setDecisionOwnerOrganization(String decisionOwnerOrganization) {
        this.decisionOwnerOrganization = decisionOwnerOrganization;
    }

    public List<InputMetaData> getInputsMetaData() {
        return inputsMetaData;
    }

    public void setInputsMetaData(List<InputMetaData> inputsMetaData) {
        this.inputsMetaData = inputsMetaData;
    }

    //#endregion

}
