package org.example.model;

import java.util.ArrayList;
import java.util.List;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;
import org.json.JSONObject;

import static java.nio.charset.StandardCharsets.UTF_8;

import com.owlike.genson.annotation.JsonProperty;

@DataType()
public class DecisionAsset {

    @Property()
    public String decisionId;

    @Property()
    public String decisionName;

    @Property()
    public String hitPolicy;

    @Property()
    private List<Input> inputs;


    @Property()
    private List<Output> outputs;

    @Property()
    private String owner;

    public DecisionAsset(@JsonProperty("decisionId") String decisionId,@JsonProperty("decisionName") String decisionName,
    @JsonProperty("hitPolicy") String hitPolicy,@JsonProperty("inputs") List<Input> inputs,@JsonProperty("outputs") List<Output> outputs) {
        this.decisionId = decisionId;
        this.decisionName = decisionName;
        this.hitPolicy = hitPolicy;
        this.inputs = inputs;
        this.outputs = outputs;
    }

    public byte[] serialize() {
        String jsonStr = new JSONObject(this).toString();
        return jsonStr.getBytes(UTF_8);
    }

    public String toJSONString() {
        return new JSONObject(this).toString();
    }

    //#region getters, setters

    public String getDecisionId() {
        return decisionId;
    }



    public void setDecisionId(String decisionId) {
        decisionId = this.decisionId;
    }



    public String getDecisionName() {
        return decisionName;
    }



    public void setDecisionName(String decisionName) {
        decisionName = this.decisionName;
    }



    public String getHitPolicy() {
        return hitPolicy;
    }



    public void setHitPolicy(String hitPolicy) {
        this.hitPolicy = hitPolicy;
    }



    public List<Input> getInputs() {
        return inputs;
    }



    public void setInputs(List<Input> inputs) {
        this.inputs = inputs;
    }



    public List<Output> getOutputs() {
        return outputs;
    }



    public void setOutputs(List<Output> outputs) {
        this.outputs = outputs;
    }



    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    //#endregion

    
    public List<Integer> evaluate(String inputName, String value) {
        for (Input  i : this.getInputs()) {
            if(i.getInputName().equals(inputName)){
                return EvaluateEngine.evaluateBoolean(i.values, value);
            }
        }
        return null;
    }
}
