/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.example;

import org.example.Helpers.PermissionLevels;
import org.example.model.DataDependencyAsset;
import org.example.model.DecisionAsset;
import org.example.model.Input;
import org.example.model.InputMetaData;
import org.example.model.PrivateInputValueAsset;
import org.example.model.ProcessAsset;
import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.contract.ContractInterface;
import org.hyperledger.fabric.contract.annotation.Contract;
import org.hyperledger.fabric.contract.annotation.Default;
import org.hyperledger.fabric.contract.annotation.Transaction;
import org.hyperledger.fabric.shim.Chaincode.Response;
import org.hyperledger.fabric.contract.annotation.Contact;
import org.hyperledger.fabric.contract.annotation.Info;
import org.hyperledger.fabric.contract.annotation.License;
import com.owlike.genson.Genson;

import io.grpc.LoadBalancer.Helper;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//#region ContractMeta
@Contract(name = "BusinessRuleExecuterContract",
    info = @Info(title = "BusinessRuleExecuter contract",
                description = "My Private Data Smart Contract",
                version = "0.0.1",
                license =
                        @License(name = "Apache-2.0",
                                url = ""),
                                contact =  @Contact(email = "privateData@example.com",
                                                name = "privateData",
                                                url = "http://privateData.me")))
//#endregion

@Default
public class DecisionAssetContract implements ContractInterface {

    private final static Genson genson = new Genson();
    public  DecisionAssetContract() {}
    
    @Transaction()
    public boolean decisionAssetExists(Context ctx, String collectionName, String decisionAssetId) {
        byte[] buffer = ctx.getStub().getPrivateDataHash(collectionName, decisionAssetId);
        return (buffer != null && buffer.length > 0);
    }

    @Transaction()
    public String createDecisionAssetFromDMN(Context ctx, String decisionAssetId) {
        boolean exists = decisionAssetExists(ctx,Helpers.getCollectionNameFromContext(ctx, PermissionLevels.Permissioned),decisionAssetId);
        if (exists) {
            throw new RuntimeException("The asset decision asset "+decisionAssetId+" already exists");
        }
        Map<String, byte[]> transientData = ctx.getStub().getTransient();
        if (transientData.size() == 0 | !transientData.containsKey("dmn")) {
            throw new RuntimeException("The decision model was not specified in transient data. Please try again.");
        }
        String xml = Helpers.recoverStringParameterFromBytes(transientData.get("dmn"));
        String ownerOrganization = Helpers.getOrgNameFromContext(ctx);

        try {
            DecisionAsset decisionAsset = Helpers.createDecisionAssetFromXml(xml);
            decisionAsset.setOwner(ownerOrganization);
            ctx.getStub().putPrivateData(Helpers.getCollectionNameFromContext(ctx, PermissionLevels.Permissioned), decisionAssetId, decisionAsset.serialize());
            //save permissions
            List<String> distinctOrganizations = Helpers.getDistinctOrganizations(decisionAsset);
            for(String distinctOrgName : distinctOrganizations){
                List<InputMetaData> inputsMetaData = new ArrayList<>();
                for(Input i:decisionAsset.getInputs()){
                    for(String orgName: i.getOrganizations()){
                        if (distinctOrgName.equals(orgName)){
                            InputMetaData inputMetaData = new InputMetaData(i);
                            inputsMetaData.add(inputMetaData);
                        }
                    }
                }
                DataDependencyAsset dataDependencyAsset = new DataDependencyAsset(decisionAssetId, ownerOrganization, inputsMetaData);
                ctx.getStub().putPrivateData(Helpers.getCollectionNameWithOrgName(distinctOrgName, PermissionLevels.Modifiable), decisionAssetId, dataDependencyAsset.serialize());
            }
            
            return decisionAsset.toJSONString();
        } catch (Exception e) {
            System.out.println("=====ERROR===== " + e.toString() + " =====ERROR=====");
            return e.toString();
        } 
    }

    @Transaction()
    public String startProcess(Context ctx, String processId, String decisionId) {
        //Probléma
        String ownerOrganization = ctx.getClientIdentity().getMSPID();
        DecisionAsset decisionAsset;
        decisionAsset = this.readDecisionAsset(ctx, decisionId, ownerOrganization);
        List<String> distinctOrganizations = Helpers.getDistinctOrganizations(decisionAsset);
        //save process to the invoker PermissionedCollection
        ProcessAsset processAsset = new ProcessAsset(processId, "started", ownerOrganization, decisionAsset.getInputs().size(), decisionId);
        ctx.getStub().putPrivateData(Helpers.getCollectionNameFromContext(ctx, PermissionLevels.Permissioned),processId, processAsset.serialize());
        //save permissions
        processAsset.setReamainingInputCount(1);
        processAsset.setStatus("input data required");
        for (String organization : distinctOrganizations) {
            ctx.getStub().putPrivateData(Helpers.getCollectionNameWithOrgName(organization, PermissionLevels.Modifiable), processId, processAsset.serialize());
        }
        return processAsset.toJSONString();
    }
    @Transaction()
    public List<Integer> addInputData(Context ctx, String processId, String inputName, String value) {
        PrivateInputValueAsset privateValueAsset = new PrivateInputValueAsset(value);
        ProcessAsset processAsset = genson.deserialize(ctx.getStub().getPrivateData(Helpers.getCollectionNameFromContext(ctx, PermissionLevels.Modifiable), processId),ProcessAsset.class);
        DataDependencyAsset dataDependencyAsset = genson.deserialize(ctx.getStub().getPrivateData(Helpers.getCollectionNameFromContext(ctx, PermissionLevels.Permissioned), processAsset.getDecisionId()), DataDependencyAsset.class);
        //type check
        String decisionOwner = dataDependencyAsset.getDecisionOwnerOrganization();
        DecisionAsset decisionAsset = readDecisionAsset(ctx, processAsset.getDecisionId(), decisionOwner);
        List<Integer> result = decisionAsset.evaluate(inputName, value);
        return result;
    }

    @Transaction()
    public String getOutput(Context ctx, String processId, String outputName) {
        return "output";
    }

    @Transaction()
    public String readProcessAsset(Context ctx, String processAssetId, String collectionName) {
        boolean exists = decisionAssetExists(ctx, collectionName,processAssetId);
        if (!exists) {
            return "The asset decision asset " + processAssetId + " does not exist";
        }
        byte[] privateData = ctx.getStub().getPrivateData(collectionName, processAssetId);
        ProcessAsset proc = genson.deserialize(privateData,ProcessAsset.class);

        return proc.toJSONString();
    }
    
    @Transaction()
    public DecisionAsset readDecisionAsset(Context ctx, String decisionAssetId, String ownerOrganization)  {
        boolean exists = decisionAssetExists(ctx, Helpers.getCollectionNameWithOrgName(ownerOrganization, PermissionLevels.Permissioned),decisionAssetId);
        if (!exists) {
            throw new RuntimeException("The asset decision asset " + decisionAssetId + " does not exist");
        }
        byte[] privateData = ctx.getStub().getPrivateData(Helpers.getCollectionNameWithOrgName(ownerOrganization, PermissionLevels.Permissioned), decisionAssetId);
        DecisionAsset dec = genson.deserialize(privateData,DecisionAsset.class);

        return dec;
    }
    /* 
    @Transaction()
    public boolean verifydecisionAsset(Context ctx, String mspid, String decisionAssetId, DecisionAsset objectToVerify) throws NoSuchAlgorithmException {

        // Convert user provided object into hash
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hashByte = digest.digest(objectToVerify.toJSONString().getBytes(StandardCharsets.UTF_8));

        String hashToVerify = new BigInteger(1, hashByte).toString(16);

        // Get hash stored on the public ledger
        byte[] pdHashBytes = ctx.getStub().getPrivateDataHash("_implicit_org_" + mspid, decisionAssetId);
        if (pdHashBytes.length == 0) {
            throw new RuntimeException("No private data hash with the key: " + decisionAssetId);
        }

        String actualHash = new BigInteger(1, pdHashBytes).toString(16);

        if (hashToVerify.equals(actualHash)) {
            return true;
        } else {
            return false;
        }
    }*/

}
